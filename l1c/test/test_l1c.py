
from ism.src.initIsm import initIsm
from ism.src.opticalPhase import opticalPhase
from ism.src.detectionPhase import detectionPhase
from ism.src.videoChainPhase import videoChainPhase
from common.io.readCube import readCube
from common.io.writeToa import writeToa
from common.io.writeToa import readToa
import numpy as np

refdir = '/home/luss/my_shared_folder/EODP_TER_2021/EODP-TS-L1C/output/'
testdir = '/home/luss/my_shared_folder/test_l1c/'

l1c_VNIR0ref = readToa(refdir, 'l1c_toa_VNIR-0.nc')
l1c_VNIR1ref = readToa(refdir, 'l1c_toa_VNIR-1.nc')
l1c_VNIR2ref = readToa(refdir, 'l1c_toa_VNIR-2.nc')
l1c_VNIR3ref = readToa(refdir, 'l1c_toa_VNIR-3.nc')
l1c_VNIR0test = readToa(testdir, 'l1c_toa_VNIR-0.nc')
l1c_VNIR1test = readToa(testdir, 'l1c_toa_VNIR-1.nc')
l1c_VNIR2test = readToa(testdir, 'l1c_toa_VNIR-2.nc')
l1c_VNIR3test = readToa(testdir, 'l1c_toa_VNIR-3.nc')

sort_l1c_VNIR0ref = np.sort(l1c_VNIR0ref)
sort_l1c_VNIR1ref = np.sort(l1c_VNIR1ref)
sort_l1c_VNIR2ref = np.sort(l1c_VNIR2ref)
sort_l1c_VNIR3ref = np.sort(l1c_VNIR3ref)
sort_l1c_VNIR0test = np.sort(l1c_VNIR0test)
sort_l1c_VNIR1test = np.sort(l1c_VNIR1test)
sort_l1c_VNIR2test = np.sort(l1c_VNIR2test)
sort_l1c_VNIR3test = np.sort(l1c_VNIR3test)

test_VNIR0 = np.subtract(sort_l1c_VNIR0test, sort_l1c_VNIR0ref)
test_VNIR1 = np.subtract(sort_l1c_VNIR1test, sort_l1c_VNIR1ref)
test_VNIR2 = np.subtract(sort_l1c_VNIR2test, sort_l1c_VNIR2ref)
test_VNIR3 = np.subtract(sort_l1c_VNIR3test, sort_l1c_VNIR3ref)

error_VNIR0 = test_VNIR0 >= 0.0001*sort_l1c_VNIR0test
Sum_error_VNIR0 = test_VNIR0.sum()
print('The total difference of the points between the test and reference is ',Sum_error_VNIR0)

error_VNIR1 = test_VNIR1 >= 0.0001*sort_l1c_VNIR1test
Sum_error_VNIR1 = test_VNIR1.sum()
print('The total difference of the points between the test and reference is ',Sum_error_VNIR1)

error_VNIR2 = test_VNIR2 >= 0.0001*sort_l1c_VNIR2test
Sum_error_VNIR2 = test_VNIR2.sum()
print('The total difference of the points between the test and reference is ',Sum_error_VNIR2)

error_VNIR3 = test_VNIR3 >= 0.0001*sort_l1c_VNIR3test
Sum_error_VNIR3 = test_VNIR3.sum()
print('The total difference of the points between the test and reference is ',Sum_error_VNIR3)


