from l1b.src.initL1b import initL1b
from common.io.writeToa import writeToa, readToa
from common.src.auxFunc import getIndexBand
from common.io.readFactor import readFactor, EQ_MULT, EQ_ADD, NC_EXT
import numpy as np
import os
import matplotlib.pyplot as plt
import os.path

refdir = '/home/luss/my_shared_folder/EODP_TER_2021/EODP-TS-L1B/output/'
testdir = '/home/luss/my_shared_folder/test_l1b/'
isrfdir = '/home/luss/my_shared_folder/EODP_TER_2021/EODP-TS-ISM/output/'

l1b_VNIR0ref = readToa(refdir, 'l1b_toa_VNIR-0.nc')
l1b_VNIR1ref = readToa(refdir, 'l1b_toa_VNIR-1.nc')
l1b_VNIR2ref = readToa(refdir, 'l1b_toa_VNIR-2.nc')
l1b_VNIR3ref = readToa(refdir, 'l1b_toa_VNIR-3.nc')
l1b_VNIR0test = readToa(testdir, 'l1b_toa_VNIR-0.nc')
l1b_VNIR1test = readToa(testdir, 'l1b_toa_VNIR-1.nc')
l1b_VNIR2test = readToa(testdir, 'l1b_toa_VNIR-2.nc')
l1b_VNIR3test = readToa(testdir, 'l1b_toa_VNIR-3.nc')

test_VNIR0 = np.subtract(l1b_VNIR0test, l1b_VNIR0ref)
test_VNIR1 = np.subtract(l1b_VNIR1test, l1b_VNIR1ref)
test_VNIR2 = np.subtract(l1b_VNIR2test, l1b_VNIR2ref)
test_VNIR3 = np.subtract(l1b_VNIR3test, l1b_VNIR3ref)

error_VNIR0 = test_VNIR0 >= 0.0001*l1b_VNIR0test
Sum_error_VNIR0 = test_VNIR0.sum()
print('The total difference of the points between the test and reference is ',Sum_error_VNIR0)

error_VNIR1 = test_VNIR1 >= 0.0001*l1b_VNIR1test
Sum_error_VNIR1 = test_VNIR1.sum()
print('The total difference of the points between the test and reference is ',Sum_error_VNIR1)

error_VNIR2 = test_VNIR2 >= 0.0001*l1b_VNIR2test
Sum_error_VNIR2 = test_VNIR2.sum()
print('The total difference of the points between the test and reference is ',Sum_error_VNIR2)

error_VNIR3 = test_VNIR3 >= 0.0001*l1b_VNIR3test
Sum_error_VNIR3 = test_VNIR3.sum()
print('The total difference of the points between the test and reference is ',Sum_error_VNIR3)

nlines = 100
ncolumns = 150

isrf_VNIR0ref = readToa(isrfdir, 'ism_toa_isrf_VNIR-0.nc')
isrf_VNIR1ref = readToa(isrfdir, 'ism_toa_isrf_VNIR-1.nc')
isrf_VNIR2ref = readToa(isrfdir, 'ism_toa_isrf_VNIR-2.nc')
isrf_VNIR3ref = readToa(isrfdir, 'ism_toa_isrf_VNIR-3.nc')

fig = plt.figure(figsize=(20,10))
plt.plot(abs(l1b_VNIR0test[int((nlines/2)),0:int(ncolumns/2)]),label='l1b_toa_VNIR_0')
plt.plot(abs(isrf_VNIR0ref[int((nlines/2)),0:int(ncolumns/2)]),label='ism_toa_isrf_VNIR_0')
plt.title('L1B TOA vs ISM ISRF TOA central ALT position for VNIR_0',fontsize=20)
#plt.xlabel('ACT frequencies f/(1/w) [-]',fontsize=16)
plt.ylabel('TOA',fontsize=16)
plt.grid()
plt.legend()
directory = '/home/luss/my_shared_folder/test_l1b/'
plt.savefig(os.path.join(directory,'L1B_ISRF_ALT_0.png'))

fig = plt.figure(figsize=(20,10))
plt.plot(abs(l1b_VNIR1test[int((nlines/2)),0:int(ncolumns/2)]),label='l1b_toa_VNIR_1')
plt.plot(abs(isrf_VNIR1ref[int((nlines/2)),0:int(ncolumns/2)]),label='ism_toa_isrf_VNIR_1')
plt.title('L1B TOA vs ISM ISRF TOA central ALT position for VNIR_1',fontsize=20)
#plt.xlabel('ACT frequencies f/(1/w) [-]',fontsize=16)
plt.ylabel('TOA',fontsize=16)
plt.grid()
plt.legend()
plt.savefig(os.path.join(directory,'L1B_ISRF_ALT_1.png'))

fig = plt.figure(figsize=(20,10))
plt.plot(abs(l1b_VNIR2test[int((nlines/2)),0:int(ncolumns/2)]),label='l1b_toa_VNIR_2')
plt.plot(abs(isrf_VNIR2ref[int((nlines/2)),0:int(ncolumns/2)]),label='ism_toa_isrf_VNIR_2')
plt.title('L1B TOA vs ISM ISRF TOA central ALT position for VNIR_2',fontsize=20)
#plt.xlabel('ACT frequencies f/(1/w) [-]',fontsize=16)
plt.ylabel('TOA',fontsize=16)
plt.grid()
plt.legend()
plt.savefig(os.path.join(directory,'L1B_ISRF_ALT_2.png'))

fig = plt.figure(figsize=(20,10))
plt.plot(abs(l1b_VNIR3test[int((nlines/2)),0:int(ncolumns/2)]),label='l1b_toa_VNIR_3')
plt.plot(abs(isrf_VNIR3ref[int((nlines/2)),0:int(ncolumns/2)]),label='ism_toa_isrf_VNIR_3')
plt.title('L1B TOA vs ISM ISRF TOA central ALT position for VNIR_3',fontsize=20)
#plt.xlabel('ACT frequencies f/(1/w) [-]',fontsize=16)
plt.ylabel('TOA',fontsize=16)
plt.grid()
plt.legend()
plt.savefig(os.path.join(directory,'L1B_ISRF_ALT_3.png'))
