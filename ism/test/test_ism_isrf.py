
from ism.src.initIsm import initIsm
from ism.src.opticalPhase import opticalPhase
from ism.src.detectionPhase import detectionPhase
from ism.src.videoChainPhase import videoChainPhase
from common.io.readCube import readCube
from common.io.writeToa import writeToa
from common.io.writeToa import readToa
import numpy as np

refdir = '/home/luss/my_shared_folder/EODP_TER_2021/EODP-TS-ISM/output/'
testdir = '/home/luss/my_shared_folder/test_ism/'

ims_isrf_VNIR0ref = readToa(refdir, 'ism_toa_isrf_VNIR-0.nc')
ims_isrf_VNIR1ref = readToa(refdir, 'ism_toa_isrf_VNIR-1.nc')
ims_isrf_VNIR2ref = readToa(refdir, 'ism_toa_isrf_VNIR-2.nc')
ims_isrf_VNIR3ref = readToa(refdir, 'ism_toa_isrf_VNIR-3.nc')
ims_isrf_VNIR0test = readToa(testdir, 'ism_toa_isrf_VNIR-0.nc')
ims_isrf_VNIR1test = readToa(testdir, 'ism_toa_isrf_VNIR-1.nc')
ims_isrf_VNIR2test = readToa(testdir, 'ism_toa_isrf_VNIR-2.nc')
ims_isrf_VNIR3test = readToa(testdir, 'ism_toa_isrf_VNIR-3.nc')

test_VNIR0 = np.subtract(ims_isrf_VNIR0test, ims_isrf_VNIR0ref)
test_VNIR1 = np.subtract(ims_isrf_VNIR1test, ims_isrf_VNIR1ref)
test_VNIR2 = np.subtract(ims_isrf_VNIR2test, ims_isrf_VNIR2ref)
test_VNIR3 = np.subtract(ims_isrf_VNIR3test, ims_isrf_VNIR3ref)

error_VNIR0 = test_VNIR0 >= 0.0001*ims_isrf_VNIR0test
Sum_error_VNIR0 = test_VNIR0.sum()
print('The total difference of the points between the test and reference is ',Sum_error_VNIR0)

error_VNIR1 = test_VNIR1 >= 0.0001*ims_isrf_VNIR1test
Sum_error_VNIR1 = test_VNIR1.sum()
print('The total difference of the points between the test and reference is ',Sum_error_VNIR1)

error_VNIR2 = test_VNIR2 >= 0.0001*ims_isrf_VNIR2test
Sum_error_VNIR2 = test_VNIR2.sum()
print('The total difference of the points between the test and reference is ',Sum_error_VNIR2)

error_VNIR3 = test_VNIR3 >= 0.0001*ims_isrf_VNIR3test
Sum_error_VNIR3 = test_VNIR3.sum()
print('The total difference of the points between the test and reference is ',Sum_error_VNIR3)


